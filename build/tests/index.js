'use strict';

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _dist = require('../dist');

var matrix = _interopRequireWildcard(_dist);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Matrix', function () {
	it('should support multiplication', function () {
		var m1 = [[1, 2], [3, 4]];

		var m2 = [[2, 0], [1, 2]];

		_assert2.default.deepEqual(matrix.multiply(m1, m2), [[4, 4], [10, 8]]);
	});
});
import assert from 'assert';
import * as matrix from '../dist';

describe('Matrix', () => {
	it('should support multiplication', () => {
		const m1 = [
			[1, 2],
			[3, 4],
		];

		const m2 = [
			[2, 0],
			[1, 2],
		];

		assert.deepEqual(matrix.multiply(m1, m2), [
			[4, 4],
			[10, 8],
		]);
	});
});

